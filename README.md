# Solution to the Game Dev exercise

This is my solution to the Fabamaq Game developer exercise. It was built using the [SFML](https://www.sfml-dev.org/) open-source and multi-platform library so that it can be played on any modern OS. 

# Building on windows
I've used Visual Studio 2015 to build the solution using the [SFML library for Visual C++ 14 (2015) - 32-bit](https://www.sfml-dev.org/download/sfml/2.4.2/).

The SFML library is linked statically so that the output executable can be ran without copying all the DLLs. The only DLL needed by imposition of SFMl is the **OpenAL32.dll** that can be found on the SFML/dll directory of this project.

The SFML lib files that this project depends on are:

 - sfml-graphics-s.lib
 - sfml-window-s.lib
 - sfml-audio-s.lib
 - sfml-system-s.lib
 
 For more information on how to setup the visual studio environment please refer to the [SFML tutorial page](https://www.sfml-dev.org/tutorials/2.4/start-vc.php)

If you wish to build on windows using g++ from MinGW, please [download the correct version of the SFML library](https://www.sfml-dev.org/download/sfml/2.4.2/) and refer to the [this guide](https://www.sfml-dev.org/tutorials/2.4/start-cb.php) on how to build the project.

Dont forget to copy the **OpenAL32.dll** file to the executable directory in order to run the project.

# Build on other Operating Systems
The code was developed using only the SFML library and other standard libraries. As so, the code can be built for any OS supported by the SFML library. 
Simply download the correct version of the SFML library for your OS and refer to the [SFML Getting Started Tutorials](https://www.sfml-dev.org/tutorials/2.4/) on how to setup the build environment. The entry point to the project is **main.cpp** file placed in the code folder.