#include "../../SFML/include/SFML/Graphics.hpp"
#include "headers/textInfo.h"
#include "headers/GameContext.h"

/// <summary>
/// Entry point of the code. Using 'main' instead of' WinMain'
/// with the sfml-main.lib. This way the project should compile
/// on Linux
/// </summary>
/// <returns>Standard exit Code</returns>
int main()
{
	// Create the game context
	GameContext mainGameContext;
	// Create the text displays for the counters
	mainGameContext.createTextDisplays();
	// Create the interface buttons
	mainGameContext.createButtons();
	// Create the sound controllers
	mainGameContext.createSounds();
	// Create the game objects (balls and squares)
	mainGameContext.createGameObjects();
	// Run the game until window close
	mainGameContext.runGameLoop();
	// Return success state
	return EXIT_SUCCESS;
}