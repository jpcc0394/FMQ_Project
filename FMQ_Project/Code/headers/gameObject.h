#pragma once

#include <string>
#include <stdlib.h>
#include "../../../SFML/include/SFML/Graphics.hpp"

/// <summary>
/// Namespace common to all entities and actors
/// </summary>
namespace entities {

	/// <summary>
	/// Generates a random RGB color for the
	/// ball filling
	/// </summary>
	/// <returns>The random color</returns>
	sf::Color generateRandomColor();

	/// <summary>
	/// Enum that specifies the available object types
	/// </summary>
	enum class ObjectType {
		Ball,
		Square
	};

	/// <summary>
	/// Template class to define basic properties of 
	/// game objects. All entities should
	/// inherit this class
	/// </summary>
	class GameObject {

	protected:
		/// <summary> The object type </summary>
		ObjectType m_type;
		/// <summary> The iteration counter </summary>
		long long m_counter;
		/// <summary> The object enabled state </summary>
		bool m_isEnabled;
		/// <summary> The object paused state </summary>
		bool m_isPaused;

	public:
		/// <summary>
		/// Default constructor. Creates a default
		/// GameObject with no set type
		/// </summary>
		GameObject() : 
			m_counter(0),
			m_isEnabled(false),
			m_isPaused(false) {}

		/// <summary>
		/// Constructor. Creates a default GameObject 
		/// of the provided type
		/// </summary>
		/// <param name="type">The game object type</param>
		GameObject(ObjectType type) : 
			m_type(type),
			m_counter(0),
			m_isEnabled(false),
			m_isPaused(false) {}

		/// <summary>
		/// Sets the GameObject type
		/// </summary>
		/// <param name="type">The game object type</param>
		void setType(ObjectType type) { this->m_type = type; }

		/// <summary>
		/// Gets the GameObject type
		/// </summary>
		/// <returns>The game object type</returns>
		ObjectType getType() const { return this->m_type; }

		/// <summary>
		/// Virtual method. Query if the object
		/// has finished it's animation
		/// </summary>
		/// <returns>The animation finished status</returns>
		virtual bool hasFinished() const { return false; }

		/// <summary>
		/// Virtual method. Resets the
		/// object's properties
		/// </summary>
		virtual void reset() {}

		/// <summary>
		/// Virtual method. Disables
		/// the object
		/// </summary>
		virtual void disable() {}

		/// <summary>
		/// Virtual method. Enables
		/// the object
		/// </summary>
		virtual void enable() {}

		/// <summary>
		/// Virtual method. Pauses
		/// the object
		/// </summary>
		virtual void pause() {}

		/// <summary>
		/// Virtual method. Unpauses
		/// the object
		/// </summary>
		virtual void resume() {}

		/// <summary>
		/// Virtual method. Updates
		/// the object properties
		/// </summary>
		virtual void update() {}

		/// <summary>
		/// Virtual method. Updates the object properties
		/// and draws it on the provided game window
		/// </summary>
		/// <param name="window">The game window to drawn on</param>
		virtual void updateAndDraw(sf::RenderWindow& window) {}
	};
}