#pragma once

#include "../../../SFML/include/SFML/Graphics.hpp"
#include "gameObject.h"

/// <summary>
/// Namespace common to all entities and actors
/// </summary>
namespace entities {

	/// <summary>
	/// Namespace to store default attributes of Square object
	/// </summary>
	namespace SQUARE_DEFAULTS {
		const sf::Vector2f intialPosition = sf::Vector2f(0, 0);
		const sf::Vector2f targetPosition = sf::Vector2f(0, 100);
		const sf::Vector2f moveStep = sf::Vector2f(3, 1);
		const float edgeSize = 10.0f;
		const int launchDelay = 0;
	}

	/// <summary>
	/// Class that handles the creation and 
	/// movement of a game square
	/// </summary>
	class Square : public GameObject {
	private:
		/// <summary> The geometrical shape </summary>
		sf::RectangleShape m_shape;
		/// <summary> The initial position </summary>
		sf::Vector2f m_initialPosition;
		/// <summary> The target position </summary>
		sf::Vector2f m_targetPosition;
		/// <summary> The move step per iteration </summary>
		sf::Vector2f m_moveStep;
		/// <summary> The launch delay in iterations </summary>
		int m_launchDelay;
		/// <summary> The object reached target state </summary>
		bool m_reachedTarget;

	public:
		/// <summary>
		/// Default constructor. Creates a square with the 
		/// default attributes
		/// </summary>
		Square();

		/// <summary>
		/// Creates a square at the given initial position
		/// </summary>
		/// <param name="initialPosition">The initial position</param>
		Square(sf::Vector2f initialPosition);

		/// <summary>
		/// Creates a square at the given initial position
		/// </summary>
		/// <param name="initialXPos">The initial position X coordinate</param>
		/// <param name="initialYPos">The initial position Y coordinate</param>
		Square(float initialXPos, float initialYPos);

		/// <summary>
		/// Creates a square at the given initial position and sets the
		/// target position
		/// </summary>
		/// <param name="initialPosition">The initial position</param>
		/// <param name="targetPosition">The target position</param>
		Square(sf::Vector2f initialPosition, sf::Vector2f targetPosition);

		/// <summary>
		/// Creates a square at the given initial position and sets the
		/// target position
		/// </summary>
		/// <param name="initialXPos">The initial position X coordinate</param>
		/// <param name="initialYPos">The initial position Y coordinate</param>
		/// <param name="targetXPos">The target position X coordinate</param>
		/// <param name="targetYPos">The target position Y coordinate</param>
		Square(float initialXPos, float initialYPos, float targetXPos, float targetYPos);

		/// <summary>
		/// Sets the square initial position
		/// </summary>
		/// <param name="initialPosition">The initial position</param>
		void setInitialPosition(sf::Vector2f initialPosition);

		/// <summary>
		/// Sets the square initial position
		/// </summary>
		/// <param name="xPos">The initial position X coordinate</param>
		/// <param name="yPos">The initial position Y coordinate</param>
		void setInitialPosition(float xPos, float yPos);

		/// <summary>
		/// Gets the square initial position
		/// </summary>
		/// <returns>The initial position</returns>
		sf::Vector2f getInitialPosition() const;

		/// <summary>
		/// Sets the square target position
		/// </summary>
		/// <param name="position">The target position</param>
		void setTargetPosition(sf::Vector2f position);

		/// <summary>
		/// Sets the square target position
		/// </summary>
		/// <param name="xPos">The target position X coordinate</param>
		/// <param name="yPos">The target position Y coordinate</param>
		void setTargetPosition(float xPos, float yPos);

		/// <summary>
		/// Gets the square target position
		/// </summary>
		/// <returns>The target position</returns>
		sf::Vector2f getTargetPosition() const;

		/// <summary>
		/// Sets the step to increment to the square
		/// position on each iteration
		/// </summary>
		/// <param name="moveStep">The move step</param>
		void setMovestep(sf::Vector2f moveStep);

		/// <summary>
		/// Sets the step to increment to the square
		/// position on each iteration
		/// </summary>
		/// <param name="deltaX">The move step in the horizontal direction</param>
		/// <param name="deltaY">The move step in the vertical direction</param>
		void setMovestep(float deltaX, float deltaY);

		/// <summary>
		/// Gets the square move step
		/// </summary>
		/// <returns>The move step</returns>
		sf::Vector2f getMovestep() const;

		/// <summary>
		/// Gets the square current position
		/// </summary>
		/// <returns>The square current position</returns>
		sf::Vector2f getCurrentPosition() const;

		/// <summary>
		/// Sets the square shape color
		/// </summary>
		/// <param name="color">The color</param>
		void setColor(sf::Color& color);

		/// <summary>
		/// Gets the square shape color
		/// </summary>
		/// <returns>The color</returns>
		sf::Color getColor() const;

		/// <summary>
		/// Sets the square shape edge size
		/// </summary>
		/// <param name="radius">The radius</param>
		void setEdgeSize(float edgeSize);

		/// <summary>
		/// Gets the square shape edge size
		/// </summary>
		/// <returns>The radius</returns>
		float getEdgeSize() const;

		/// <summary>
		/// Sets the square launchDelay. This delay corresponds to
		/// the number of iterations to wait before starting
		/// the square animation
		/// </summary>
		/// <param name="launchDelay">The launch delay in iterations</param>
		void setLauchDelay(int launchDelay);

		/// <summary>
		/// Gets the square launchDelay. This delay corresponds to
		/// the number of iterations to wait before starting
		/// the square animation
		/// </summary>
		/// <returns>The launch delay in iterations</returns>
		int getLaunchDelay() const;

		/// <summary>
		/// Query if the square has reached the 
		/// set target
		/// </summary>
		/// <returns>The query result. True if the square has reached the target</returns>
		bool hasFinished() const override;

		/// <summary>
		/// Resets the square attributes. More specifically, 
		/// resets the control flags and the places
		/// the square shape at the initial position
		/// </summary>
		void reset() override;

		/// <summary>
		/// Disables the square so it won't be drawn
		/// </summary>
		void disable() override;

		/// <summary>
		/// Enables the square to be drawn
		/// </summary>
		void enable() override;

		/// <summary>
		/// Pauses the square animation
		/// </summary>
		void pause() override;

		/// <summary>
		/// Resumes the square animation
		/// </summary>
		void resume() override;

		/// <summary>
		/// Updates the square position. The update depends
		/// on the square's control flags states. A square object
		/// that is disabled or paused won't move
		/// </summary>
		void update() override;

		/// <summary>
		/// Updates the square state and draws it on the given window.
		/// Only enables squares can be drawn
		/// </summary>
		/// <param name="window">Reference to the game window</param>
		void updateAndDraw(sf::RenderWindow& window) override;
	};
}