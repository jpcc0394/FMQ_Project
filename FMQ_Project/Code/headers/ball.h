#pragma once

#include "../../../SFML/include/SFML/Graphics.hpp"
#include "gameObject.h"

/// <summary>
/// Namespace common to all entities and actors
/// </summary>
namespace entities {

	/// <summary>
	/// Namespace to store default attributes of Ball object
	/// </summary>
	namespace BALL_DEFAULTS {
		const sf::Vector2f intialPosition = sf::Vector2f(0, 0);
		const sf::Vector2f targetPosition = sf::Vector2f(0, 100);
		const sf::Vector2f moveStep = sf::Vector2f(3, 1);
		const float radius = 10.0f;
		const int launchDelay = 0;
	}

	/// <summary>
	/// Class that handles the creation and 
	/// movement of a game ball
	/// </summary>
	class Ball : public GameObject {
	private:
		/// <summary> The geometrical shape </summary>
		sf::CircleShape m_shape;
		/// <summary> The initial position </summary>
		sf::Vector2f m_initialPosition;
		/// <summary> The target position </summary>
		sf::Vector2f m_targetPosition;
		/// <summary> The move step per iteration </summary>
		sf::Vector2f m_moveStep;
		/// <summary> The launch delay in iterations </summary>
		int m_launchDelay;
		/// <summary> The object reached target state </summary>
		bool m_reachedTarget;

	public:
		/// <summary>
		/// Default constructor. Creates a ball with the 
		/// default attributes
		/// </summary>
		Ball();

		/// <summary>
		/// Creates a ball at the given initial position
		/// </summary>
		/// <param name="initialPosition">The initial position</param>
		Ball(sf::Vector2f initialPosition);

		/// <summary>
		/// Creates a ball at the given initial position
		/// </summary>
		/// <param name="initialXPos">The initial position X coordinate</param>
		/// <param name="initialYPos">The initial position Y coordinate</param>
		Ball(float initialXPos, float initialYPos);

		/// <summary>
		/// Creates a ball at the given initial position and sets the
		/// target position
		/// </summary>
		/// <param name="initialPosition">The initial position</param>
		/// <param name="targetPosition">The target position</param>
		Ball(sf::Vector2f initialPosition, sf::Vector2f targetPosition);

		/// <summary>
		/// Creates a ball at the given initial position and sets the
		/// target position
		/// </summary>
		/// <param name="initialXPos">The initial position X coordinate</param>
		/// <param name="initialYPos">The initial position Y coordinate</param>
		/// <param name="targetXPos">The target position X coordinate</param>
		/// <param name="targetYPos">The target position Y coordinate</param>
		Ball(float initialXPos, float initialYPos, float targetXPos, float targetYPos);

		/// <summary>
		/// Sets the ball initial position
		/// </summary>
		/// <param name="initialPosition">The initial position</param>
		void setInitialPosition(sf::Vector2f initialPosition);

		/// <summary>
		/// Sets the ball initial position
		/// </summary>
		/// <param name="xPos">The initial position X coordinate</param>
		/// <param name="yPos">The initial position Y coordinate</param>
		void setInitialPosition(float xPos, float yPos);

		/// <summary>
		/// Gets the ball initial position
		/// </summary>
		/// <returns>The initial position</returns>
		sf::Vector2f getInitialPosition() const;

		/// <summary>
		/// Sets the ball target position
		/// </summary>
		/// <param name="position">The target position</param>
		void setTargetPosition(sf::Vector2f position);

		/// <summary>
		/// Sets the ball target position
		/// </summary>
		/// <param name="xPos">The target position X coordinate</param>
		/// <param name="yPos">The target position Y coordinate</param>
		void setTargetPosition(float xPos, float yPos);

		/// <summary>
		/// Gets the ball target position
		/// </summary>
		/// <returns>The target position</returns>
		sf::Vector2f getTargetPosition() const;

		/// <summary>
		/// Sets the step to increment to the ball
		/// position on each iteration
		/// </summary>
		/// <param name="moveStep">The move step</param>
		void setMovestep(sf::Vector2f moveStep);

		/// <summary>
		/// Sets the step to increment to the ball
		/// position on each iteration
		/// </summary>
		/// <param name="deltaX">The move step in the horizontal direction</param>
		/// <param name="deltaY">The move step in the vertical direction</param>
		void setMovestep(float deltaX, float deltaY);

		/// <summary>
		/// Gets the ball move step
		/// </summary>
		/// <returns>The move step</returns>
		sf::Vector2f getMovestep() const;

		/// <summary>
		/// Gets the ball current position
		/// </summary>
		/// <returns>The ball current position</returns>
		sf::Vector2f getCurrentPosition() const;

		/// <summary>
		/// Sets the ball shape color
		/// </summary>
		/// <param name="color">The color</param>
		void setColor(sf::Color& color);

		/// <summary>
		/// Gets the ball shape color
		/// </summary>
		/// <returns>The color</returns>
		sf::Color getColor() const;

		/// <summary>
		/// Sets the ball shape radius
		/// </summary>
		/// <param name="radius">The radius</param>
		void setRadius(float radius);

		/// <summary>
		/// Gets the ball shape radius
		/// </summary>
		/// <returns>The radius</returns>
		float getRadius() const;

		/// <summary>
		/// Sets the ball launchDelay. This delay corresponds to
		/// the number of iterations to wait before starting
		/// the ball animation
		/// </summary>
		/// <param name="launchDelay">The launch delay in iterations</param>
		void setLauchDelay(int launchDelay);

		/// <summary>
		/// Gets the ball launchDelay. This delay corresponds to
		/// the number of iterations to wait before starting
		/// the ball animation
		/// </summary>
		/// <returns>The launch delay in iterations</returns>
		int getLaunchDelay() const;

		/// <summary>
		/// Query if the ball has reached the 
		/// set target
		/// </summary>
		/// <returns>The query result. True if the ball has reached the target</returns>
		bool hasFinished() const override;

		/// <summary>
		/// Resets the ball attributes. More specifically, 
		/// resets the control flags and the places
		/// the ball shape at the initial position
		/// </summary>
		void reset() override;

		/// <summary>
		/// Disables the ball so it won't be drawn
		/// </summary>
		void disable() override;

		/// <summary>
		/// Enables the ball to be drawn
		/// </summary>
		void enable() override;

		/// <summary>
		/// Pauses the ball animation
		/// </summary>
		void pause() override;

		/// <summary>
		/// Resumes the ball animation
		/// </summary>
		void resume() override;

		/// <summary>
		/// Updates the ball position. The update depends
		/// on the ball's control flags states. A Ball object
		/// that is disabled or paused won't move
		/// </summary>
		void update() override;

		/// <summary>
		/// Updates the ball state and draws it on the given window.
		/// Only enables Balls can be drawn
		/// </summary>
		/// <param name="window">Reference to the game window</param>
		void updateAndDraw(sf::RenderWindow& window) override;

	};

} /// Namespace entities