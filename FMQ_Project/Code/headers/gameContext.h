#pragma once

#include <string>
#include "../../../SFML/include/SFML/Graphics.hpp"
#include "../../../SFML/include/SFML/Audio.hpp"
#include "textInfo.h"
#include "button.h"
#include "gameObject.h"
#include "ball.h"
#include "square.h"
#include "player.h"
#include "soundController.h"

/// <summary>
/// Namespace that defines the default game properties
/// </summary>
namespace GAMECONTEXT_DEFAULTS {
	const int windowWidth = 1280;
	const int windowHeight = 720;
	const int errorWindowWidth = 400;
	const int errorWindowHeight = 200;
	const std::string windowName = "Wiggly Shapes";
	const std::string errorWindowName = "Game Error";
	const int numberOfObjects = 50;
	const int objectSpacing = 20;
	const float objectHorizontalOffset = 20;
	const float objectVerticalOffset = 200;
	const float objectVerticalTarget = 550;
	const int objectDelay = 5;
}

/// <summary>
/// Class that handles the context of the game and the user events
/// It is also responsible for maintaining the render loop
/// </summary>
class GameContext {
private:
	/**************** SFML objects *******************/ 
	/// <summary> The game window where objects are rendered </summary>
	sf::RenderWindow m_gameWindow;
	/// <summary> The font common to all text in the game window </summary>
	sf::Font m_textFont;

	/********* Game drawables and player data *******/
	/// <summary> The handler for the remove credits button </summary>
	tools::Button m_removeCreditBtn;
	/// <summary> The handler for the start credits button </summary>
	tools::Button m_startBtn;
	/// <summary> The handler for the add credits button </summary>
	tools::Button m_addCreditBtn;
	/// <summary> The handler for the player credits count text </summary>
	tools::TextInfo m_playerCreditsTxt;
	/// <summary> The handler for the removed credits count text </summary>
	tools::TextInfo m_removedCreditsTxt;
	/// <summary> The handler for the play count text </summary>
	tools::TextInfo m_playCountTxt;
	/// <summary> The handler for the player information </summary>
	entities::Player m_player;

	/************* Sound Controllers ****************/
	/// <summary> The handler for the start game sound player </summary>
	tools::SoundController m_gameStartSound;
	/// <summary> The handler for the start playing sound player </summary>
	tools::SoundController m_gamePlayingSound;
	/// <summary> The handler for the end game sound player </summary>
	tools::SoundController m_gameEndSound;

	/************* Game context variables ***********/
	/// <summary> The horizontal resolution of the game window </summary>
	int m_windowWidth;
	/// <summary> The vertical resolution of the game window </summary>
	int m_windowHeight;
	/// <summary> The game window title </summary>
	std::string m_windowName;
	/// <summary> Represents the number of objects to be animated </summary>
	int m_numberOfObjects;
	/// <summary> Represents the vector of game objects to be animated </summary>
	std::vector<entities::GameObject*> m_gameObjects;
	/// <summary> Indicates if game is being played or not </summary>
	bool m_isPlaying;
	/// <summary> Indicates if the game is paused or not </summary>
	bool m_isPaused;
	/// <summary> Indicates if the game has an open error window </summary>
	bool m_errorWindowOpen;

public:
	/// <summary>
	/// Default constructor. Creates a game context with the default parameters
	/// </summary>
	GameContext();

	/// <summary>
	/// Constructor. Creates a game window with a specific resolution
	/// </summary>
	/// <param name="width">The horizontal resolution</param>
	/// <param name="height">The vertical resolution</param>
	GameContext(int width, int height);

	/// <summary>
	/// Sets the number of objects to be animated
	/// </summary>
	/// <param name="numberOfObjects">The number of objects</param>
	void setNumberOfObjects(int numberOfObjects);

	/// <summary>
	/// Gets the number of animated objects
	/// </summary>
	/// <returns></returns>
	int getNumberOfObjects();

	/// <summary>
	/// Creates the text handlers for credits and
	/// play counter information
	/// </summary>
	void createTextDisplays();

	/// <summary>
	/// Creates the handlers for the remove credits,
	/// start and add credits buttons
	/// </summary>
	void createButtons();

	/// <summary>
	/// Creates the handlers for the game sounds
	/// </summary>
	void createSounds();

	/// <summary>
	/// Calculates the radius of the balls based on the game
	/// window dimensions and the desired space between
	/// each pair of balls
	/// </summary>
	/// <param name="spacing">Spacing in pixels between each ball</param>
	/// <param name="horizontalOffset">The offset of the left-most ball to the game window border</param>
	/// <returns></returns>
	float calculateBallRadius(int spacing, float horizontalOffset);

	/// <summary>
	/// Creates the game objects for the game animation
	/// </summary>
	void createGameObjects(
		int spacing = GAMECONTEXT_DEFAULTS::objectSpacing,
		float horizontalOffset = GAMECONTEXT_DEFAULTS::objectHorizontalOffset,
		float verticalOffset = GAMECONTEXT_DEFAULTS::objectVerticalOffset,
		float verticalTarget = GAMECONTEXT_DEFAULTS::objectVerticalTarget,
		int launchDelay = GAMECONTEXT_DEFAULTS::objectDelay);

	/// <summary>
	/// Checks if the mouse pointer is inside the bounds
	/// of an object
	/// </summary>
	/// <param name="btnBounds">The object's bounds</param>
	/// <returns>True if the mouse is inside the object's bounds, false if not</returns>
	bool checkMouseBetweenBound(sf::FloatRect& btnBounds);

	/// <summary>
	/// Creates an error window to inform the player 
	/// that the game can only be played if there 
	/// is at least one player credit
	/// </summary>
	void handleNoCreditsError();

	/// <summary>
	/// Starts the game if the user has credits
	/// and updates the interface properties
	/// </summary>
	void startGame();

	/// <summary>
	/// Pauses the game
	/// </summary>
	void pauseGame();

	/// <summary>
	/// Resumes the game from paused state
	/// </summary>
	void resumeGame();

	/// <summary>
	/// Resets the game context state by 
	/// reseting the objects and the 
	/// control flags
	/// </summary>
	void resetGame();

	/// <summary>
	/// Checks the condition for the game to end 
	/// (all objects finishing their animation)
	/// and triggers the game end events
	/// </summary>
	void checkAndHandleGameEnd();

	/// <summary>
	/// Handles the mouse left clicks and determines
	/// which events should take place
	/// </summary>
	void handleMouseClicked();

	/// <summary>
	/// Handles the mouse movement events and updates 
	/// the interface if necessary
	/// </summary>
	void handleMouseMoved();

	/// <summary>
	/// Updates the string of the text containers with the
	/// current credits and plays count values
	/// </summary>
	void updateTextContainers();

	/// <summary>
	/// Draws the objects on the game window
	/// </summary>
	void updateScreen();

	/// <summary>
	/// Executes main game loop until
	/// user closes the game window
	/// </summary>
	void runGameLoop();
};