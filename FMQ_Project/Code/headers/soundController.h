#pragma once

#include <string>
#include "../../../SFML/include/SFML/Audio.hpp"

/// <summary>
/// Namespace common to tools libraries
/// </summary>
namespace tools {

	/// <summary>
	/// Namespace with the default properties for a SoundController object
	/// </summary>
	namespace SOUND_DEFAULTS {
		const std::string gameStartFilePath = "../Assets/Sounds/GameStart.ogg";
		const std::string gamePlayingFilePath = "../Assets/Sounds/GamePlaying.flac";
		const std::string gameEndFilePath = "../Assets/Sounds/GameEnd.wav";
	}

	class SoundController {
	private:
		/// <summary> The sound buffer object that keeps the audio file </summary>
		sf::SoundBuffer m_soundBuffer;
		/// <summary> The sound player </summary>
		sf::Sound m_sound;
		/// <summary> The path to the audio file </summary>
		std::string m_filePath;
	public:
		/// <summary>
		/// Default constructor
		/// </summary>
		SoundController();

		/// <summary>
		/// Constructor. Creates the SoundController object and loads
		/// an audio file. Additionally, can also set the audio loop
		/// property 
		/// </summary>
		/// <param name="filePath">The path to the audio file</param>
		/// <param name="loop">The play sound on loop/repeat flag value</param>
		SoundController(std::string filePath, bool loop = false);

		/// <summary>
		/// Loads an audio file from disk
		/// </summary>
		/// <param name="filePath">The path to the audio file</param>
		void loadFromFile(std::string filePath);

		/// <summary>
		/// Gets the path to the audio file
		/// </summary>
		/// <returns></returns>
		std::string getFilePath() const;

		/// <summary>
		/// Sets the play sound on loop property
		/// </summary>
		/// <param name="loop">The play sound on loop/repeat flag value</param>
		void setLoop(bool loop);

		/// <summary>
		/// Gets the play sound on loop property
		/// </summary>
		/// <returns>The play sound on loop/repeat flag value</returns>
		bool getLoop() const;

		/// <summary>
		/// Plays or resumes the sound
		/// </summary>
		void play();
		
		/// <summary>
		/// Stops the sound if it being played
		/// </summary>
		void stop();

		/// <summary>
		/// Pauses the sound if is being played
		/// </summary>
		void pause();
	};
} /// Namespace tools