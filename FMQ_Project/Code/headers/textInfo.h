#pragma once

#include <string>
#include "../../../SFML/include/SFML/Graphics.hpp"

/// <summary>
/// Namespace common to tools libraries
/// </summary>
namespace tools {

	/// <summary>
	/// Namespace with the default properties for a TextInfo object
	/// </summary>
	namespace TEXTINFO_DEFAULTS {
		const sf::Color textColor = sf::Color::Red;
		const sf::Text::Style textStyle = sf::Text::Style::Regular;
		const int characterSize = 20;
		const sf::Vector2f position = sf::Vector2f(0, 0);
		const std::string textValue = "EMPTY TEXT";
		const std::string fontPath = "../Assets/Fonts/Bubblegum.ttf";
	}

	/// <summary>
	/// Class to handle text info banners on game space. This class should be used to create all text information.
	/// The idea behind is to simplify the creation of text containers by having default arguments for all the 
	/// mandatory parameters
	/// </summary>
	class TextInfo {

	private:
		/// <summary>Font used by text container</summary>
		sf::Font m_textFont;
		/// <summary>Text container object</summary>
		sf::Text m_textContainer;

	public:
		/// <summary>
		/// Default constructor
		/// </summary>
		TextInfo();

		/// <summary>
		/// Create the object with from a string
		/// </summary>
		/// <param name="textValue">The initial text to display</param>
		TextInfo(std::string textValue);

		/// <summary>
		/// Create the object from a string with an existing Font object
		/// </summary>
		/// <param name="textValue">The initial text to display</param>
		/// <param name="font">The reference to the existing Font object</param>
		TextInfo(std::string textValue, sf::Font& font);

		/// <summary>
		/// Create the object by copying an existing one
		/// </summary>
		/// <param name="container">The object to copy</param>
		TextInfo(sf::Text container);

		/// <summary>
		/// Gets the Text object
		/// </summary>
		/// <returns>The container</returns>
		sf::Text getContainer() const;

		/// <summary>
		/// Sets the new value of the text string
		/// </summary>
		/// <param name="textValue">The value to set as the text string</param>
		void setTextValue(std::string textValue);

		/// <summary>
		/// Gets the text string displayed by the container
		/// </summary>
		/// <returns>The text string</returns>
		std::string getTextValue() const;

		/// <summary>
		/// Sets the position of the text container
		/// </summary>
		/// <param name="position">Th position vector</param>
		void setPosition(sf::Vector2f position);

		/// <summary>
		/// Sets the position of the text container
		/// </summary>
		/// <param name="xPos">The X coordinate</param>
		/// <param name="yPos">The Y Coordinate</param>
		void setPosition(float xPos, float yPos);

		/// <summary>
		/// Gets the position of the upper left coordinate of the text container
		/// </summary>
		/// <returns>The upper left corner position</returns>
		sf::Vector2f getPosition() const;

		/// <summary>
		/// Sets the color of the displayed text
		/// </summary>
		/// <param name="color">The color</param>
		void setColor(sf::Color color);

		/// <summary>
		/// Gets the color object of the displayed text
		/// </summary>
		/// <returns>The text color</returns>
		sf::Color getColor() const;

		/// <summary>
		/// Sets the character size of the displayed text
		/// </summary>
		/// <param name="characterSize">The character size</param>
		void setCharacterSize(int characterSize);

		/// <summary>
		/// Gets the character size of the displayed text
		/// </summary>
		/// <returns>The character size</returns>
		int getCharacterSize() const;

		/// <summary>
		/// Sets the style of the displayed text
		/// Multiple styles can be piped together
		/// </summary>
		/// <param name="style">The style</param>
		void setTextStyle(sf::Text::Style style);

		/// <summary>
		/// Gets the style of the displayed text
		/// </summary>
		/// <returns>The text style</returns>
		sf::Text::Style getTextStyle() const;

		/// <summary>
		/// Sets the font for the text container by loading it from a path
		/// </summary>
		/// <param name="fontPath">The path to the font file</param>
		void setFont(std::string fontPath);

		/// <summary>
		/// Sets the font for the text container by reference to
		/// an existing font object
		/// </summary>
		/// <param name="font">The reference to the font object</param>
		void setFont(sf::Font& font);

		/// <summary>
		/// Gets the font of the text container
		/// </summary>
		/// <returns>The font object</returns>
		sf::Font getFont() const;

		/// <summary>
		/// Draws the text on the provided 
		/// window
		/// </summary>
		/// <param name="window">The window to updateAndDraw on</param>
		void draw(sf::RenderWindow& window);

	};

} /// Namespace tools