#pragma once

#include <string>
#include "../../../SFML/include/SFML/Graphics.hpp"
#include "textInfo.h"

/// <summary>
/// Namespace common to tools libraries
/// </summary>
namespace tools {

	/// <summary>
	/// Namespace that defines the default button properties
	/// </summary>
	namespace BUTTON_DEFAULTS {
		const sf::Vector2f size(175, 75);
		const int lineThickness = 7;
		const std::string text = "BUTTON";
	}

	/// <summary>
	/// Class that handles buttons and stores their information.
	/// This class should be used create all button objects
	/// </summary>
	class Button {

	private:
		/// <summary> The TextInfo object that displays the button's text </summary>
		TextInfo m_textHandler;
		/// <summary> the geometrical shape of the button</summary>
		sf::RectangleShape m_buttonShape;
		/// <summary> The text of the button </summary>
		std::string m_buttonText;
		/// <summary> The button's enabled state </summary>
		bool m_isEnabled;
		/// <summary> The button's mouse over state </summary>
		bool m_isMouseOver;

	public:
		/// <summary>
		/// Default constructor. Creates a button with the 
		/// default properties
		/// </summary>
		Button();

		/// <summary>
		/// Constructor. Creates a button at the specified position
		/// </summary>
		/// <param name="xPos">The x coordinate</param>
		/// <param name="yPos">The y coordinate</param>
		Button(float xPos, float yPos);

		/// <summary>
		/// Sets the reference to the font to be 
		/// used for the button's text
		/// </summary>
		/// <param name="font">The font object</param>
		void setFont(sf::Font& font);

		/// <summary>
		/// Sets the button's text
		/// </summary>
		/// <param name="text">The text</param>
		void setText(std::string text);

		/// <summary>
		/// Sets the button's position
		/// </summary>
		/// <param name="position">The position vector</param>
		void setPosition(sf::Vector2f position);

		/// <summary>
		/// Set's the button's position
		/// </summary>
		/// <param name="xPos">The X coordinate</param>
		/// <param name="yPos">The Y coordinate</param>
		void setPosition(float xPos, float yPos);

		/// <summary>
		/// Gets the text container for the 
		/// button's text
		/// </summary>
		/// <returns>The text container</returns>
		sf::Text getTextContainer();

		/// <summary>
		/// Gets the shape of the button
		/// </summary>
		/// <returns>The button's shape object</returns>
		sf::RectangleShape getShape();

		/// <summary>
		/// Checks the button's enabled status
		/// </summary>
		/// <returns>The button's status</returns>
		bool isEnabled();

		/// <summary>
		/// Enables the button
		/// </summary>
		void enable();

		/// <summary>
		/// Disables the button
		/// </summary>
		void disable();

		/// <summary>
		/// Gets the button's shape bounds
		/// </summary>
		/// <returns>The button's bounds</returns>
		sf::FloatRect getBounds();

		/// <summary>
		/// Sets the button's mouse over status
		/// </summary>
		/// <param name="mouseOver">The mouse over status</param>
		void setMouseOver(bool mouseOver);
		 
		/// <summary>
		/// Updates the button design based on the
		/// button's status
		/// </summary>
		void update();

		/// <summary>
		/// Draws the button on the provided 
		/// window
		/// </summary>
		/// <param name="window">The window to updateAndDraw on</param>
		void drawAndUpdate(sf::RenderWindow& window);

	};

} /// Namespace tools