#pragma once

/// <summary>
/// Namespace common to all entities and actors
/// of the project
/// </summary>
namespace entities {

	/// <summary>
	/// Namespace that stores the default player values
	/// </summary>
	namespace PLAYER_DEFAULTS {
		const int credits = 0;
	}

	/// <summary>
	/// Class to handle player data and actions
	/// </summary>
	class Player {
	private:
		/// <summary> The number of the available credits for the player </summary>
		int m_playerCreditCount;
		/// <summary> The number of credits removed from the game </summary>
		int m_removedCreditCount;
		/// <summary> The number of games played by the player </summary>
		int m_playCount;
	public:
		/// <summary>
		/// Default class constructor. Creates a player object
		/// with the default values
		/// </summary>
		Player();

		/// <summary>
		/// Increments the number of player credits by one
		/// </summary>
		void addCredit();

		/// <summary>
		/// Gets the number of player credits
		/// </summary>
		/// <returns>The number of player credits</returns>
		int getPlayerCreditCount();

		/// <summary>
		/// Removes the player credits from the game
		/// </summary>
		void removeGameCredits();

		/// <summary>
		/// Gets the total number of credits removed from the game
		/// </summary>
		/// <returns>The number of credits removed from the game</returns>
		int getRemovedCreditCount();

		/// <summary>
		/// Handles the start of a game by the player. Decreases 
		/// the number of available player credits
		/// </summary>
		void engagePlay();

		/// <summary>
		/// Handles the end of a game by the player. Increases
		/// the play count
		/// </summary>
		void endPlay();

		/// <summary>
		/// Gets the number of games played
		/// </summary>
		/// <returns>the number of games played</returns>
		int getPlayCount();
	};

} /// Namespace entities