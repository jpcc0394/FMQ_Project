#include "../headers/button.h"

namespace tools {
	Button::Button() {
		this->m_textHandler.setTextValue(BUTTON_DEFAULTS::text);
		this->m_buttonShape.setSize(BUTTON_DEFAULTS::size);
		this->m_buttonShape.setPosition(m_textHandler.getPosition());
		this->m_buttonShape.setOutlineThickness(BUTTON_DEFAULTS::lineThickness);
		this->m_buttonShape.setOutlineColor(sf::Color::Black);
		this->m_isEnabled = true;
	}

	Button::Button(float xPos, float yPos) {
		this->m_textHandler.setTextValue(BUTTON_DEFAULTS::text);
		this->m_textHandler.setPosition(xPos, yPos);
		this->m_buttonShape.setSize(BUTTON_DEFAULTS::size);
		this->m_buttonShape.setPosition(m_textHandler.getPosition());
		this->m_buttonShape.setOutlineThickness(BUTTON_DEFAULTS::lineThickness);
		this->m_buttonShape.setOutlineColor(sf::Color::Black);
		this->m_isEnabled = true;
	}

	void Button::setFont(sf::Font& font) {
		// Pass the font to the text handler
		this->m_textHandler.setFont(font);
	}

	void Button::setText(std::string text) {
		this->m_textHandler.setTextValue(text);
	}

	void Button::setPosition(sf::Vector2f position) {
		// Set the text position
		this->m_textHandler.setPosition(position);
		// Set the shape position
		this->m_buttonShape.setPosition(position);
	}

	void Button::setPosition(float xPos, float yPos) {
		// Set the text position
		this->m_textHandler.setPosition(xPos, yPos);
		// Calculate the bounding rectangle of the button to center with text
		sf::FloatRect btnRect = this->getBounds();
		this->m_buttonShape.setOrigin(btnRect.left + btnRect.width / 2.0f,
									  btnRect.top + btnRect.height / 2.0f);
		// Set the shape position
		this->m_buttonShape.setPosition(xPos, yPos);
	}

	sf::Text Button::getTextContainer() {
		return this->m_textHandler.getContainer();
	}

	sf::RectangleShape Button::getShape() {
		return this->m_buttonShape;
	}

	bool Button::isEnabled() {
		return this->m_isEnabled;
	}

	void Button::enable() {
		this->m_isEnabled = true;
	}

	void Button::disable() {
		// Disable the button and change it's color
		this->m_isEnabled = false;
		this->m_buttonShape.setFillColor(sf::Color::Black);
	}

	sf::FloatRect Button::getBounds() {
		return this->m_buttonShape.getGlobalBounds();
	}

	void Button::setMouseOver(bool mouseOver) {
		this->m_isMouseOver = mouseOver;
	}

	void Button::update() {
		// Update only if the button is enabled
		if (this->m_isEnabled) {
			if (this->m_isMouseOver) {
				// When the mouse is inside the button change the color
				this->m_buttonShape.setFillColor(sf::Color(192, 192, 192, 100));
			}
			else {
				// When the mouse leaves the button reste the color
				this->m_buttonShape.setFillColor(sf::Color(0,0,0,0));
			}
		}
	}

	void Button::drawAndUpdate(sf::RenderWindow& window) {
		// Update the button status
		this->update();
		// Draw the shape
		window.draw(this->m_buttonShape);
		// Draw the text
		this->m_textHandler.draw(window);
	}

}