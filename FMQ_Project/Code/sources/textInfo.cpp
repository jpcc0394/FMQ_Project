#include "../headers/textInfo.h"

namespace tools {
	TextInfo::TextInfo() {
		/// Fill the container with the default values
		this->setTextValue(TEXTINFO_DEFAULTS::textValue);
		this->setCharacterSize(TEXTINFO_DEFAULTS::characterSize);
		this->setColor(sf::Color::Black);
		this->setTextStyle(TEXTINFO_DEFAULTS::textStyle);
		this->setPosition(TEXTINFO_DEFAULTS::position);
		this->setFont(TEXTINFO_DEFAULTS::fontPath);
	}

	TextInfo::TextInfo(std::string textValue) {
		/// Fill the container with the given string and the default values
		this->setCharacterSize(TEXTINFO_DEFAULTS::characterSize);
		this->setTextStyle(TEXTINFO_DEFAULTS::textStyle);
		this->setPosition(TEXTINFO_DEFAULTS::position);
		this->setFont(TEXTINFO_DEFAULTS::fontPath);
		this->setColor(sf::Color::Black);
		this->setTextValue(textValue);
	}

	TextInfo::TextInfo(std::string textValue, sf::Font& font) {
		/// Fill the container with the given string and font and the default values
		this->setCharacterSize(TEXTINFO_DEFAULTS::characterSize);
		this->setTextStyle(TEXTINFO_DEFAULTS::textStyle);
		this->setPosition(TEXTINFO_DEFAULTS::position);
		this->setFont(font);
		this->setColor(sf::Color::Black);
		this->setTextValue(textValue);
	}

	TextInfo::TextInfo(sf::Text container) {
		/// Copy the container
		this->m_textContainer = container;
	}

	sf::Text TextInfo::getContainer() const {
		return this->m_textContainer;
	}

	void TextInfo::setTextValue(std::string textValue) {
		this->m_textContainer.setString(textValue);
	}

	std::string TextInfo::getTextValue() const {
		return this->m_textContainer.getString();
	}

	void TextInfo::setPosition(sf::Vector2f position) {
		this->m_textContainer.setPosition(position);
	}

	void TextInfo::setPosition(float xPos, float yPos) {
		// Center the text with the bounding rectangle
		sf::FloatRect textRect = this->m_textContainer.getLocalBounds();
		this->m_textContainer.setOrigin(textRect.left + textRect.width / 2.0f,
										textRect.top + textRect.height / 2.0f);
		this->m_textContainer.setPosition(xPos, yPos);
	}

	sf::Vector2f TextInfo::getPosition() const {
		return this->m_textContainer.getPosition();
	}

	void TextInfo::setColor(sf::Color color) {
		this->m_textContainer.setFillColor(color);
	}

	sf::Color TextInfo::getColor() const {
		return m_textContainer.getFillColor();
	}

	void TextInfo::setCharacterSize(int characterSize) {
		this->m_textContainer.setCharacterSize(characterSize);
	}

	int TextInfo::getCharacterSize() const {
		return this->m_textContainer.getCharacterSize();
	}

	void TextInfo::setTextStyle(sf::Text::Style style) {
		this->m_textContainer.setStyle(style);
	}

	sf::Text::Style TextInfo::getTextStyle() const {
		return sf::Text::Style(m_textContainer.getStyle());
	}

	void TextInfo::setFont(std::string fontPath) {
		// Set the font if it is successfully loaded
		if (this->m_textFont.loadFromFile(fontPath))
			this->m_textContainer.setFont(m_textFont);
	}

	void TextInfo::setFont(sf::Font& font) {
		this->m_textContainer.setFont(font);
	}

	sf::Font TextInfo::getFont() const {
		return *this->m_textContainer.getFont();
	}

	void TextInfo::draw(sf::RenderWindow& window) {
		window.draw(this->m_textContainer);
	}

}