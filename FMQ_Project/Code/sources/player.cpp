#include "../headers/player.h"

namespace entities {
	Player::Player() :
		m_playerCreditCount(PLAYER_DEFAULTS::credits),
		m_removedCreditCount(0),
		m_playCount(0) {}

	void Player::addCredit() {
		this->m_playerCreditCount++;
	}

	int Player::getPlayerCreditCount() {
		return this->m_playerCreditCount;
	}

	void Player::removeGameCredits() {
		// Add the current player credits to the removed credits
		this->m_removedCreditCount += this->m_playerCreditCount;
		// Reset the player credits
		this->m_playerCreditCount = 0;
	}

	int Player::getRemovedCreditCount() {
		return this->m_removedCreditCount;
	}

	void Player::engagePlay() {
		// Remove a credit
		this->m_playerCreditCount--;
	}
	
	void Player::endPlay() {
		// Increment play count;
		this->m_playCount++;
	}

	int Player::getPlayCount() {
		return this->m_playCount;
	}
}