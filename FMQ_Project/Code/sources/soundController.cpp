#include "../headers/soundController.h"

namespace tools {
	SoundController::SoundController() {}

	SoundController::SoundController(std::string filePath, bool loop) :
		m_filePath(filePath) {
		this->loadFromFile(this->m_filePath);
	}

	void SoundController::loadFromFile(std::string filePath) {
		// Set the file path
		this->m_filePath = filePath;
		if (this->m_soundBuffer.loadFromFile(this->m_filePath))
			// Set the buffer if the file is found
			this->m_sound.setBuffer(this->m_soundBuffer);
		else
			throw(std::exception("Can't find file"));
	}

	std::string SoundController::getFilePath() const {
		return this->m_filePath;
	}

	void SoundController::setLoop(bool loop) {
		this->m_sound.setLoop(loop);
	}

	bool SoundController::getLoop() const {
		return this->m_sound.getLoop();
	}

	void SoundController::play() {
		this->m_sound.play();
	}

	void SoundController::pause() {
		this->m_sound.pause();
	}

	void SoundController::stop() {
		this->m_sound.stop();
	}
}