#include "../headers/GameContext.h"

GameContext::GameContext() : 
	m_windowWidth(GAMECONTEXT_DEFAULTS::windowWidth),
	m_windowHeight(GAMECONTEXT_DEFAULTS::windowHeight),
	m_windowName(GAMECONTEXT_DEFAULTS::windowName),
	m_numberOfObjects(GAMECONTEXT_DEFAULTS::numberOfObjects),
	m_isPlaying(false),
	m_errorWindowOpen(false) {
	// Create settings to enable anti-aliasing
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;
	// Create the game window with default attributes
	this->m_gameWindow.create(
		sf::VideoMode(m_windowWidth, m_windowHeight),
		m_windowName,
		sf::Style::Default,
		settings);
	// Set the FPS lock
	this->m_gameWindow.setFramerateLimit(60);
	// Create a font that will be shared by all text containers
	this->m_textFont.loadFromFile(tools::TEXTINFO_DEFAULTS::fontPath);
}

GameContext::GameContext(int width, int height) :
	m_windowWidth(width),
	m_windowHeight(height),
	m_windowName(GAMECONTEXT_DEFAULTS::windowName),
	m_numberOfObjects(GAMECONTEXT_DEFAULTS::numberOfObjects),
	m_isPlaying(false),
	m_errorWindowOpen(false) {
	// Create settings to enable anti-aliasing
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;
	// Create the game window with given resolution
	this->m_gameWindow.create(
		sf::VideoMode(m_windowWidth, m_windowHeight),
		m_windowName,
		sf::Style::Default,
		settings);
	// Set the FPS lock
	this->m_gameWindow.setFramerateLimit(60);
	// Create a font that will be shared by all text containers
	this->m_textFont.loadFromFile(tools::TEXTINFO_DEFAULTS::fontPath);
}

void GameContext::setNumberOfObjects(int numberOfObjects) {
	m_numberOfObjects = numberOfObjects;
}

int GameContext::getNumberOfObjects() {
	return m_numberOfObjects;
}

void GameContext::createTextDisplays() {
	// Create the text container for the game credits indicator
	this->m_removedCreditsTxt = tools::TextInfo("Game Credits: ", this->m_textFont);
	this->m_removedCreditsTxt.setPosition(175, 20);
	// Create the text container for the player credits indicator
	this->m_playerCreditsTxt = tools::TextInfo("Player Credits: ", this->m_textFont);
	this->m_playerCreditsTxt.setPosition(this->m_windowWidth/2.0f, 20);
	// Create the text container for the plays counter indicator
	this->m_playCountTxt = tools::TextInfo("Number of plays: ", this->m_textFont);
	this->m_playCountTxt.setPosition(this->m_windowWidth - 200, 20);
}

void GameContext::createButtons() {
	// Create the remove credits Button
	m_removeCreditBtn.setText("Remove credits");
	m_removeCreditBtn.setFont(this->m_textFont);
	m_removeCreditBtn.setPosition(200, 650);
	// Create the start Button
	m_startBtn.setText("Start");
	m_startBtn.setFont(this->m_textFont);
	m_startBtn.setPosition(this->m_windowWidth / 2.0f, 650);
	// Create the add credits Button
	m_addCreditBtn.setText("Add credits");
	m_addCreditBtn.setFont(this->m_textFont);
	m_addCreditBtn.setPosition(this->m_windowWidth - 200, 650);
}

void GameContext::createSounds() {
	// Set the controllers for the sound of game start
	this->m_gameStartSound.loadFromFile(tools::SOUND_DEFAULTS::gameStartFilePath);
	// Set the controllers for the sound of game playing
	this->m_gamePlayingSound.loadFromFile(tools::SOUND_DEFAULTS::gamePlayingFilePath);
	// The game playing sound is looped
	this->m_gamePlayingSound.setLoop(true);
	// Set the controllers for the sound of game end
	this->m_gameEndSound.loadFromFile(tools::SOUND_DEFAULTS::gameEndFilePath);
}


float GameContext::calculateBallRadius(int spacing, float horizontalOffset) {
	// Calculate the total width that the ball area will occupy
	float ballDisplayAreaWidth = this->m_windowWidth - 2.0f * horizontalOffset;
	// Calculate the width per ball (diameter) considering the ball spacing
	float widthPerBall = (ballDisplayAreaWidth - GAMECONTEXT_DEFAULTS::objectSpacing * this->m_numberOfObjects) / this->m_numberOfObjects;
	// Return the radius
	return widthPerBall / 2.0f;
}

void GameContext::createGameObjects(
	int spacing,
	float horizontalOffset,
	float verticalOffset,
	float verticalTarget,
	int launchDelay) {
	// Get the ball radius 
	float ballRadius = this->calculateBallRadius(spacing, horizontalOffset);
	// Create all the even numbered balls at the correct position
	int lastLaunchIndex = 0;
	for (int ballIndex = 0; ballIndex < this->m_numberOfObjects; ballIndex+=2) {
		// Calculate the ball center coordinate on the horizontal axis
		int ballXCenter = horizontalOffset + ballIndex*(ballRadius*2.0f + spacing);
		// Update the launch index
		lastLaunchIndex++;
		// Create the ball with the properties
		entities::Ball* newBall = new entities::Ball(ballXCenter, verticalOffset, ballXCenter, verticalTarget);
		newBall->setRadius(ballRadius);
		newBall->setLauchDelay(lastLaunchIndex*launchDelay);
		// Add the ball to the game objects vector
		this->m_gameObjects.push_back(newBall);
	}
	// Create all the odd numbered squares at the correct position
	for (int ballIndex = 1; ballIndex < this->m_numberOfObjects; ballIndex += 2) {
		// Calculate the square center coordinate on the horizontal axis
		int squareXCenter = horizontalOffset + ballIndex*(ballRadius*2.0f + spacing);
		// Update the launch index
		lastLaunchIndex++;
		// Create the square with the properties
		entities::Square* newSquare = new entities::Square(squareXCenter, verticalOffset, squareXCenter, verticalTarget);
		// Using the radius of the balls for the square's edge size
		newSquare->setEdgeSize(ballRadius*2.0f);
		newSquare->setLauchDelay(lastLaunchIndex*launchDelay);
		// Add the square to the game objects vector
		this->m_gameObjects.push_back(newSquare);
	}
}

bool GameContext::checkMouseBetweenBound(sf::FloatRect& btnBounds) {
	// Get the mouse position inside the game window
	sf::Vector2i mousePos = sf::Mouse::getPosition(this->m_gameWindow);
	// Check if the mouse is inside the button box and return the result
	return btnBounds.contains(sf::Vector2f(mousePos.x, mousePos.y));
}

void GameContext::handleNoCreditsError() {
	// Create a new window to show the error
	sf::RenderWindow errorWindow(
		sf::VideoMode(
			GAMECONTEXT_DEFAULTS::errorWindowWidth, 
			GAMECONTEXT_DEFAULTS::errorWindowHeight),
		GAMECONTEXT_DEFAULTS::errorWindowName,
		sf::Style::Default);
	// Create a text container for the error message
	tools::TextInfo errorText("You need at least 1 credit to play", this->m_textFont);
	// Set the error text position at the center of the window
	errorText.setPosition(
		GAMECONTEXT_DEFAULTS::errorWindowWidth / 2.0f,
		GAMECONTEXT_DEFAULTS::errorWindowHeight / 2.0f);
	// Set the helper flag so that the main window can't be closed
	this->m_errorWindowOpen = true;
	// Loop the error window until user closes
	while (errorWindow.isOpen())
	{
		// Fetch last event
		sf::Event event;
		while (errorWindow.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				// Handle window close
				errorWindow.close();
				break;
			case sf::Event::KeyPressed:
				// Handle close with escape key
				if (event.key.code == sf::Keyboard::Escape)
					errorWindow.close();
				break;
			default:
				break;
			}
		}
		errorWindow.clear(sf::Color::White);
		errorText.draw(errorWindow);
		errorWindow.display();
	}
	// Reset the helper flag so that the main window can operate
	this->m_errorWindowOpen = false;
}


void GameContext::startGame() {
	// Stop the game end sound if it is playing
	this->m_gameEndSound.stop();
	// Start the game only if the user has credits
	if (m_player.getPlayerCreditCount() > 0) {
		//  Play the game start sound
		this->m_gameStartSound.play();
		// Set the playing flag
		this->m_isPlaying = true;
		// Update the player object
		this->m_player.engagePlay();
		// Disable credits buttons
		this->m_removeCreditBtn.disable();
		this->m_addCreditBtn.disable();
		// Enable the game objects
		for (entities::GameObject* gameObject : this->m_gameObjects) {
			gameObject->enable();
		}
		// Play the game playing sound
		this->m_gamePlayingSound.play();
		// Change the start button text
		this->m_startBtn.setText("Pause");
	}
	else {
		// Display error message
		this->handleNoCreditsError();
	}
}

void GameContext::pauseGame() {
	// Set the paused flag
	this->m_isPaused = true;
	// Pause the game playing sound
	this->m_gamePlayingSound.pause();
	// Stop the game start sound
	this->m_gameStartSound.stop();
	// Pause the game objects
	for (entities::GameObject* gameObject : this->m_gameObjects) {
		gameObject->pause();
	}
	// Change the start button text
	this->m_startBtn.setText("Resume");
}

void GameContext::resumeGame() {
	// Set the paused flag
	this->m_isPaused = false;
	// Resume the game playing sound
	this->m_gamePlayingSound.play();
	// Unpause the game objects
	for (entities::GameObject* gameObject : this->m_gameObjects) {
		gameObject->resume();
	}
	// Change the start button text
	this->m_startBtn.setText("Pause");
}

void GameContext::resetGame() {
	// Reset all the game objects
	for (entities::GameObject* gameObject : this->m_gameObjects) {
		gameObject->reset();
	}
	// Reset the credits buttons
	this->m_removeCreditBtn.enable();
	this->m_addCreditBtn.enable();
	// Reset the flags
	this->m_isPaused = false;
	this->m_isPlaying = false;
	// Reset the start button text
	this->m_startBtn.setText("Start");
}

void GameContext::checkAndHandleGameEnd() {
	if (this->m_isPlaying) {
		// Set flag to test game end condition
		bool gameEnded = true;
		// Check if all the game objects have finished the animation
		for (entities::GameObject* gameObject : this->m_gameObjects) {
			if (!gameObject->hasFinished()) {
				// If any object hasn't finished,
				// the game doesn't end yet
				gameEnded = false;
				break;
			}
		}
		// Determine if the game should be reset
		if (gameEnded) {
			// Stop the game playing sound
			this->m_gamePlayingSound.stop();
			// Play the game end sound
			this->m_gameEndSound.play();
			// Increment the play count
			this->m_player.endPlay();
			// Reset the game status
			this->resetGame();
		}
	}
}

void GameContext::handleMouseClicked() {
	// Check which button was clicked and perform the corresponding action
	if (this->checkMouseBetweenBound(this->m_removeCreditBtn.getBounds()) && m_removeCreditBtn.isEnabled()) {
		// If the remove credits button remove the player credits from the game
		this->m_player.removeGameCredits();
	}
	else if (this->checkMouseBetweenBound(this->m_addCreditBtn.getBounds()) && m_addCreditBtn.isEnabled()) {
		// If the add credits button is clicked increment player credits
		this->m_player.addCredit();
	}
	else if (this->checkMouseBetweenBound(this->m_startBtn.getBounds())) {
		// If the start button is clicked call game start / pause routine
		if (!this->m_isPlaying) {
			this->startGame();
		}
		else if (!this->m_isPaused) {
			this->pauseGame();
		}
		else {
			this->resumeGame();
		}
	}
}

void GameContext::handleMouseMoved() {
	// Check if mouse is over remove credits button
	this->m_removeCreditBtn.setMouseOver(this->checkMouseBetweenBound(this->m_removeCreditBtn.getBounds()));
	// Check if mouse is over start button
	this->m_startBtn.setMouseOver(this->checkMouseBetweenBound(this->m_startBtn.getBounds()));
	// Check if mouse is over add credits button
	this->m_addCreditBtn.setMouseOver(this->checkMouseBetweenBound(this->m_addCreditBtn.getBounds()));
}

void GameContext::updateTextContainers() {
	// Update the player credits text
	this->m_removedCreditsTxt.setTextValue("Removed credits: " + std::to_string(this->m_player.getRemovedCreditCount()));
	// Update the player credits text
	this->m_playerCreditsTxt.setTextValue("Player Credits: " + std::to_string(this->m_player.getPlayerCreditCount()));
	// Update the play count text
	this->m_playCountTxt.setTextValue("Plays counter: " + std::to_string(this->m_player.getPlayCount()));
}

void GameContext::updateScreen() {
	// Update the text of the counters
	this->updateTextContainers();

	// Clear the window
	this->m_gameWindow.clear(sf::Color::White);

	// Draw the text information
	this->m_removedCreditsTxt.draw(this->m_gameWindow);
	this->m_playerCreditsTxt.draw(this->m_gameWindow);
	this->m_playCountTxt.draw(this->m_gameWindow);

	// Draw the buttons
	this->m_removeCreditBtn.drawAndUpdate(this->m_gameWindow);
	this->m_startBtn.drawAndUpdate(this->m_gameWindow);
	this->m_addCreditBtn.drawAndUpdate(this->m_gameWindow);

	// Draw the game objects
	for (entities::GameObject* gameObject : this->m_gameObjects) {
		gameObject->updateAndDraw(this->m_gameWindow);
	}
}

void GameContext::runGameLoop() {
	// Run loop until user closes the window
	while (this->m_gameWindow.isOpen())
	{
		// Fetch last event
		sf::Event event;
		// Handle event if there is no error window open
		while (this->m_gameWindow.pollEvent(event) && !this->m_errorWindowOpen)
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				// Handle window close
				this->m_gameWindow.close();
				break;
			case sf::Event::KeyPressed:
				// Handle close with escape key
				if(event.key.code == sf::Keyboard::Escape)
					this->m_gameWindow.close();
				break;
			case sf::Event::MouseMoved:
				// Handle mouse movement
				this->handleMouseMoved();
				break;
			case sf::Event::MouseButtonReleased:
				// Handle left clicks with the mouse
				if(event.mouseButton.button == sf::Mouse::Left)
					this->handleMouseClicked();
				break;
			default:
				break;
			}
		}
		// Check and handle game end condition
		this->checkAndHandleGameEnd();
		// Update the screen content
		this->updateScreen();
		// Display the game window
		this->m_gameWindow.display();
	}
}