#include "../headers/GameContext.h"

namespace entities {
	sf::Color generateRandomColor() {
		// Generate three numbers between 0 and 255 for color channel values
		int red = rand() % 256;
		int green = rand() % 256;
		int blue = rand() % 256;
		return sf::Color(red, green, blue, 255);
	}
}