#include "../headers/ball.h"

namespace entities {

	Ball::Ball() : 
		GameObject(ObjectType::Ball),
		m_initialPosition(BALL_DEFAULTS::intialPosition),
		m_targetPosition(BALL_DEFAULTS::targetPosition),
		m_moveStep(BALL_DEFAULTS::moveStep),
		m_launchDelay(BALL_DEFAULTS::launchDelay),
		m_reachedTarget(false) {
		// Create the ball shape 
		this->m_shape = sf::CircleShape(BALL_DEFAULTS::radius);
		this->m_shape.setPosition(m_initialPosition);
		this->m_shape.setFillColor(generateRandomColor());
	}

	Ball::Ball(sf::Vector2f initialPosition) :
		GameObject(ObjectType::Ball),
		m_initialPosition(initialPosition),
		m_targetPosition(BALL_DEFAULTS::targetPosition),
		m_moveStep(BALL_DEFAULTS::moveStep),
		m_launchDelay(BALL_DEFAULTS::launchDelay),
		m_reachedTarget(false) {
		// Create the ball shape 
		this->m_shape = sf::CircleShape(BALL_DEFAULTS::radius);
		this->m_shape.setPosition(m_initialPosition);
		this->m_shape.setFillColor(generateRandomColor());
	}

	Ball::Ball(float initialXPos, float initialYPos) : 
		GameObject(ObjectType::Ball),
		m_initialPosition(initialXPos, initialYPos),
		m_targetPosition(BALL_DEFAULTS::targetPosition),
		m_moveStep(BALL_DEFAULTS::moveStep),
		m_launchDelay(BALL_DEFAULTS::launchDelay),
		m_reachedTarget(false) {
		// Create the ball shape 
		this->m_shape = sf::CircleShape(BALL_DEFAULTS::radius);
		this->m_shape.setPosition(m_initialPosition);
		this->m_shape.setFillColor(generateRandomColor());
	}

	Ball::Ball(sf::Vector2f initialPosition, sf::Vector2f targetPosition) : 
		GameObject(ObjectType::Ball),
		m_initialPosition(initialPosition),
		m_targetPosition(targetPosition),
		m_moveStep(BALL_DEFAULTS::moveStep),
		m_launchDelay(BALL_DEFAULTS::launchDelay),
		m_reachedTarget(false) {
		// Create the ball shape 
		this->m_shape = sf::CircleShape(BALL_DEFAULTS::radius);
		this->m_shape.setPosition(m_initialPosition);
		this->m_shape.setFillColor(generateRandomColor());
	}

	Ball::Ball(float initialXPos, float initialYPos, float targetXPos, float targetYPos) : 
		GameObject(ObjectType::Ball),
		m_initialPosition(initialXPos, initialYPos),
		m_targetPosition(targetXPos, targetYPos),
		m_moveStep(BALL_DEFAULTS::moveStep),
		m_launchDelay(BALL_DEFAULTS::launchDelay),
		m_reachedTarget(false) {
		// Create the ball shape 
		this->m_shape = sf::CircleShape(BALL_DEFAULTS::radius);
		this->m_shape.setPosition(m_initialPosition);
		this->m_shape.setFillColor(generateRandomColor());
	}

	void Ball::setInitialPosition(sf::Vector2f position) {
		// Store the initial position
		this->m_initialPosition = position;
		// Set the shape's position
		this->m_shape.setPosition(this->m_initialPosition);
	}

	void Ball::setInitialPosition(float xPos, float yPos) {
		// Store the initial position
		this->m_initialPosition = sf::Vector2f(xPos, yPos);
		// Set the shape's position
		this->m_shape.setPosition(this->m_initialPosition);
	}

	sf::Vector2f Ball::getInitialPosition() const {
		return this->m_initialPosition;
	}

	void Ball::setTargetPosition(sf::Vector2f position) {
		// Store the target position
		this->m_targetPosition = position;
	}

	void Ball::setTargetPosition(float xPos, float yPos) {
		// Store the target position
		this->m_targetPosition = sf::Vector2f(xPos, yPos);
	}

	sf::Vector2f Ball::getTargetPosition() const {
		return this->m_targetPosition;
	}

	void Ball::setMovestep(sf::Vector2f moveStep) {
		// Store the move step
		this->m_moveStep = moveStep;
	}

	void Ball::setMovestep(float deltaX, float deltaY) {
		// Store the move step
		this->m_moveStep = sf::Vector2f(deltaX, deltaY);
	}

	sf::Vector2f Ball::getMovestep() const {
		return this->m_moveStep;
	}

	sf::Vector2f Ball::getCurrentPosition() const {
		return this->m_shape.getPosition();
	}


	void Ball::setColor(sf::Color& color) {
		this->m_shape.setFillColor(color);
	}

	sf::Color Ball::getColor() const {
		return this->m_shape.getFillColor();
	}

	void Ball::setRadius(float radius) {
		this->m_shape.setRadius(radius);
	}

	float Ball::getRadius() const {
		return this->m_shape.getRadius();
	}

	void Ball::setLauchDelay(int launchDelay) {
		this->m_launchDelay = launchDelay;
	}

	int Ball::getLaunchDelay() const {
		return this->m_launchDelay;
	}


	bool Ball::hasFinished() const {
		return this->m_reachedTarget;
	}

	void Ball::reset() {
		// Reset the iterations counter
		this->m_counter = 0;
		// Reset the ball position
		this->m_shape.setPosition(this->m_initialPosition);
		// Disable the ball
		this->m_isEnabled = false;
		// Reset the paused state
		this->m_isPaused = false;
		// Reset the reached target flag
		this->m_reachedTarget = false;
	}

	void Ball::disable() {
		this->m_isEnabled = false;
	}

	void Ball::enable() {
		this->m_isEnabled = true;
	}

	void Ball::pause() {
		this->m_isPaused = true;
	}

	void Ball::resume() {
		this->m_isPaused = false;
	}

	void Ball::update() {
		// Update only if the game is not paused
		if (!this->m_isPaused && this->m_isEnabled) {
			this->m_counter++;
			if (this->m_counter >= this->m_launchDelay && !this->m_reachedTarget) {
				// Move the ball
				this->m_shape.move(this->m_moveStep);
				// Update the move step so that the ball wiggles
				this->m_moveStep.x = -this->m_moveStep.x;
				// Check if the ball reach the target
				if (this->getCurrentPosition().y >= this->m_targetPosition.y) {
					// Update the flag
					this->m_reachedTarget = true;
				}
			}
		}
	}

	void Ball::updateAndDraw(sf::RenderWindow& window) {
		// Only updateAndDraw if ball is enabled
		if (this->m_isEnabled) {
			// First update the position
			this->update();
			window.draw(this->m_shape);
		}
	}
} /// Namespace entities