#include "../headers/square.h"

namespace entities {

	Square::Square() :
		GameObject(ObjectType::Square),
		m_initialPosition(SQUARE_DEFAULTS::intialPosition),
		m_targetPosition(SQUARE_DEFAULTS::targetPosition),
		m_moveStep(SQUARE_DEFAULTS::moveStep),
		m_launchDelay(SQUARE_DEFAULTS::launchDelay),
		m_reachedTarget(false) {
		// Create the square shape 
		this->m_shape = sf::RectangleShape(sf::Vector2f(SQUARE_DEFAULTS::edgeSize, SQUARE_DEFAULTS::edgeSize));
		this->m_shape.setPosition(m_initialPosition);
		this->m_shape.setFillColor(generateRandomColor());
	}

	Square::Square(sf::Vector2f initialPosition) :
		GameObject(ObjectType::Square),
		m_initialPosition(initialPosition),
		m_targetPosition(SQUARE_DEFAULTS::targetPosition),
		m_moveStep(SQUARE_DEFAULTS::moveStep),
		m_launchDelay(SQUARE_DEFAULTS::launchDelay),
		m_reachedTarget(false) {
		// Create the square shape 
		this->m_shape = sf::RectangleShape(sf::Vector2f(SQUARE_DEFAULTS::edgeSize, SQUARE_DEFAULTS::edgeSize));
		this->m_shape.setPosition(m_initialPosition);
		this->m_shape.setFillColor(generateRandomColor());
	}

	Square::Square(float initialXPos, float initialYPos) :
		GameObject(ObjectType::Square),
		m_initialPosition(initialXPos, initialYPos),
		m_targetPosition(SQUARE_DEFAULTS::targetPosition),
		m_moveStep(SQUARE_DEFAULTS::moveStep),
		m_launchDelay(SQUARE_DEFAULTS::launchDelay),
		m_reachedTarget(false) {
		// Create the square shape 
		this->m_shape = sf::RectangleShape(sf::Vector2f(SQUARE_DEFAULTS::edgeSize, SQUARE_DEFAULTS::edgeSize));
		this->m_shape.setPosition(m_initialPosition);
		this->m_shape.setFillColor(generateRandomColor());
	}

	Square::Square(sf::Vector2f initialPosition, sf::Vector2f targetPosition) :
		GameObject(ObjectType::Square),
		m_initialPosition(initialPosition),
		m_targetPosition(targetPosition),
		m_moveStep(SQUARE_DEFAULTS::moveStep),
		m_launchDelay(SQUARE_DEFAULTS::launchDelay),
		m_reachedTarget(false) {
		// Create the square shape 
		this->m_shape = sf::RectangleShape(sf::Vector2f(SQUARE_DEFAULTS::edgeSize, SQUARE_DEFAULTS::edgeSize));
		this->m_shape.setPosition(m_initialPosition);
		this->m_shape.setFillColor(generateRandomColor());
	}

	Square::Square(float initialXPos, float initialYPos, float targetXPos, float targetYPos) :
		GameObject(ObjectType::Square),
		m_initialPosition(initialXPos, initialYPos),
		m_targetPosition(targetXPos, targetYPos),
		m_moveStep(SQUARE_DEFAULTS::moveStep),
		m_launchDelay(SQUARE_DEFAULTS::launchDelay),
		m_reachedTarget(false) {
		// Create the square shape 
		this->m_shape = sf::RectangleShape(sf::Vector2f(SQUARE_DEFAULTS::edgeSize, SQUARE_DEFAULTS::edgeSize));
		this->m_shape.setPosition(m_initialPosition);
		this->m_shape.setFillColor(generateRandomColor());
	}

	void Square::setInitialPosition(sf::Vector2f position) {
		// Store the initial position
		this->m_initialPosition = position;
		// Set the shape's position
		this->m_shape.setPosition(this->m_initialPosition);
	}

	void Square::setInitialPosition(float xPos, float yPos) {
		// Store the initial position
		this->m_initialPosition = sf::Vector2f(xPos, yPos);
		// Set the shape's position
		this->m_shape.setPosition(this->m_initialPosition);
	}

	sf::Vector2f Square::getInitialPosition() const {
		return this->m_initialPosition;
	}

	void Square::setTargetPosition(sf::Vector2f position) {
		// Store the target position
		this->m_targetPosition = position;
	}

	void Square::setTargetPosition(float xPos, float yPos) {
		// Store the target position
		this->m_targetPosition = sf::Vector2f(xPos, yPos);
	}

	sf::Vector2f Square::getTargetPosition() const {
		return this->m_targetPosition;
	}

	void Square::setMovestep(sf::Vector2f moveStep) {
		// Store the move step
		this->m_moveStep = moveStep;
	}

	void Square::setMovestep(float deltaX, float deltaY) {
		// Store the move step
		this->m_moveStep = sf::Vector2f(deltaX, deltaY);
	}

	sf::Vector2f Square::getMovestep() const {
		return this->m_moveStep;
	}

	sf::Vector2f Square::getCurrentPosition() const {
		return this->m_shape.getPosition();
	}


	void Square::setColor(sf::Color& color) {
		this->m_shape.setFillColor(color);
	}

	sf::Color Square::getColor() const {
		return this->m_shape.getFillColor();
	}

	void Square::setEdgeSize(float edgeSize) {
		this->m_shape.setSize(sf::Vector2f(edgeSize, edgeSize));
	}

	float Square::getEdgeSize() const {
		return this->m_shape.getSize().x;
	}

	void Square::setLauchDelay(int launchDelay) {
		this->m_launchDelay = launchDelay;
	}

	int Square::getLaunchDelay() const {
		return this->m_launchDelay;
	}


	bool Square::hasFinished() const {
		return this->m_reachedTarget;
	}

	void Square::reset() {
		// Reset the iterations counter
		this->m_counter = 0;
		// Reset the square position
		this->m_shape.setPosition(this->m_initialPosition);
		// Disable the square
		this->m_isEnabled = false;
		// Reset the paused state
		this->m_isPaused = false;
		// Reset the reached target flag
		this->m_reachedTarget = false;
	}

	void Square::disable() {
		this->m_isEnabled = false;
	}

	void Square::enable() {
		this->m_isEnabled = true;
	}

	void Square::pause() {
		this->m_isPaused = true;
	}

	void Square::resume() {
		this->m_isPaused = false;
	}

	void Square::update() {
		// Update only if the game is not paused
		if (!this->m_isPaused && this->m_isEnabled) {
			this->m_counter++;
			if (this->m_counter >= this->m_launchDelay && !this->m_reachedTarget) {
				// Move the square
				this->m_shape.move(this->m_moveStep);
				// Update the move step so that the square wiggles
				this->m_moveStep.x = -this->m_moveStep.x;
				// Check if the square reach the target
				if (this->getCurrentPosition().y >= this->m_targetPosition.y) {
					// Update the flag
					this->m_reachedTarget = true;
				}
			}
		}
	}

	void Square::updateAndDraw(sf::RenderWindow& window) {
		// Only updateAndDraw if square is enabled
		if (this->m_isEnabled) {
			// First update the position
			this->update();
			window.draw(this->m_shape);
		}
	}
} /// Namespace entities